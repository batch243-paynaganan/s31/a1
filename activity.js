/* 
1. What directive is used by node.js in loading the modules it needs?
    The import directive.

2. What node.js module contains a method for server creation?
    The http module.

3. What is the method of the http object responsible for creating a server using node.js?
    The createServer()method.

4. What method of the response object allows us to set status codes and content types?
    The writeHead()method.

5. Where will the console.log() output its contents when run in node.js?
    The terminal/git bash.

6. What property of the request object contains the address' endpoint
    The request.url method.

*/